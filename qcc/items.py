# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class QccItem(scrapy.Item):
    dateTime = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()


# 公司列表信息
class CompanylistItem(scrapy.Item):
    logo = scrapy.Field()
    # 公司名称
    company_name = scrapy.Field()
    # 法人
    fddbr = scrapy.Field()
    # 注册资本
    zczb = scrapy.Field()
    # 成立时间
    clsj = scrapy.Field()
    # 电话
    dh = scrapy.Field()
    # 邮箱
    email = scrapy.Field()
    # 地址
    dz = scrapy.Field()
    # 经营状态
    jyzt = scrapy.Field()
    # 详细信息页面
    detail_url = scrapy.Field()

# 工商信息
class GSXXItem(scrapy.Item):
    company_id = scrapy.Field()
    company_name = scrapy.Field()
    fddbr = scrapy.Field()
    djzt = scrapy.Field()
    clrq = scrapy.Field()
    zczb = scrapy.Field()
    sjzb = scrapy.Field()
    hzrq = scrapy.Field()
    xydm = scrapy.Field()
    jgdm = scrapy.Field()
    gszch = scrapy.Field()
    nsrsbh = scrapy.Field()
    qydm = scrapy.Field()
    sshy = scrapy.Field()
    qylx = scrapy.Field()
    yyqx = scrapy.Field()
    djjg = scrapy.Field()
    rygm = scrapy.Field()
    cbrs = scrapy.Field()
    ssdq = scrapy.Field()
    cym = scrapy.Field()
    ywm = scrapy.Field()
    qydz = scrapy.Field()
    jyfw = scrapy.Field()

# 股东信息
class GDXXItem(scrapy.Item):
    company_id = scrapy.Field()
    # 股东名称
    gdmc = scrapy.Field()
    # 持股比例
    cgbl = scrapy.Field()
    # 认缴出资额
    rjcze = scrapy.Field()
    # 认缴出资日期
    rjczrq = scrapy.Field()

# 主要人员
class ZYXXItem(scrapy.Item):
    company_id = scrapy.Field()
    name = scrapy.Field()
    job = scrapy.Field()
    cgbl = scrapy.Field()
    sygf = scrapy.Field()
