# selenuim uitl
from scrapy.http import HtmlResponse, request
from selenium import webdriver
import time

from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from html.parser import HTMLParser # 将字符串格式的html文本转成html
import re
from lxml import etree



from qcc import LogUtil


class SelenuimUitl():

    def __init__(self):
        # 调用插件
        chrome_options = Options()
        # 不打开浏览器
        chrome_options.add_argument('headless')
        chrome_options.add_experimental_option('excludeSwitches', ['enable-automation'])
        self.browser = webdriver.Chrome(options=chrome_options,executable_path="/Users/fangzhong/PycharmProjects/chromedriver")
        self.browser.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
            "source": """
            Object.defineProperty(navigator, 'webdriver', {
              get: () => undefined
            })
          """
        })

    def __del__(self):
        self.browser.close()

    # 登录企查查、保存cookie
    def login(self,phone,pwd):
        self.browser.get("https://www.qichacha.com/user_login")
        time.sleep(3)

        self.browser.find_element_by_xpath('//*[@id = "normalLogin"]').click()  # 转到登录界面
        self.browser.find_element_by_xpath('//*[@id = "nameNormal"]').send_keys(phone)  # 账号
        self.browser.find_element_by_xpath('//*[@id = "pwdNormal"]').send_keys(pwd)  # 密码
        time.sleep(1)
        button = self.browser.find_element_by_id("nc_1_n1z")
        action = ActionChains(self.browser)  # 实例化一个action对象
        action.click_and_hold(button).perform()  # perform()用来执行ActionChains中存储的行为
        action.move_by_offset(348, 0).perform()
        action.reset_actions()

        time.sleep(5)
        self.browser.find_element_by_xpath('//*[@id="user_login_normal"]/button').click()  # 点击登录
        time.sleep(5)

        dictCookies = self.browser.get_cookies()
        return dictCookies

    def area(self):
        self.browser.get("https://www.qichacha.com/user_login")
        time.sleep(1)

        self.browser.find_element_by_xpath('//*[@id = "normalLogin"]').click()  # 转到登录界面
        self.browser.find_element_by_xpath('//*[@id = "nameNormal"]').send_keys('13771800574')  # 账号
        self.browser.find_element_by_xpath('//*[@id = "pwdNormal"]').send_keys('qcc123456')  # 密码

        button = self.browser.find_element_by_id("nc_1_n1z")
        action = ActionChains(self.browser)  # 实例化一个action对象
        action.click_and_hold(button).perform()  # perform()用来执行ActionChains中存储的行为
        action.move_by_offset(348, 0).perform()
        action.reset_actions()

        time.sleep(1)
        self.browser.find_element_by_xpath('//*[@id="user_login_normal"]/button').click()  # 点击登录

        time.sleep(3)
        self.browser.get('https://www.qcc.com/web/elib/newcompany')
        time.sleep(2)
        return HtmlResponse(request.url, body=self.browser.page_source)

    # 点击企业名
    def qymClick(self):
        self.browser.get('https://www.qcc.com/')
        time.sleep(1)
        self.browser.find_element_by_xpath('//*[@id = "searchkey"]').send_keys('上海')
        self.browser.find_element_by_xpath('//*[@id = "indexSearchForm"]/div/span/input').click()
        time.sleep(4)
        aa=self.browser.find_element_by_xpath('//*[@class="pills-after"]/div[0]/label')
        time.sleep(2)
        return HtmlResponse(request.url, body=self.browser.page_source)

        # 西南交大
    def xnjdClick(self):
        self.browser.get("https://sso.xnjd.cn/login?service=http%3A%2F%2Fstudy.xnjd.cn%2F")
        time.sleep(3)
        #
        self.browser.find_element_by_xpath('//*[@id="username"]').send_keys('19941625')  # 转到登录界面
        self.browser.find_element_by_xpath('//*[@id="password"]').send_keys('910509')  # 账号
        self.browser.find_element_by_xpath('//*[@id="login-page"]/body/section/div/div/div/div[2]/form/section[4]/input[4]').click()  # 密码
        time.sleep(3)

        t=self.browser.get("https://study.xnjd.cn/study/Homework_list.action")


        # self.browser.find_element_by_xpath('//*[@id="fast-links"]/div[2]/ul[1]/li[1]/a[2]').click()
        time.sleep(3)

        pageSource=self.browser.page_source
        html = etree.HTML(pageSource)  # 初始化生成一个XPath解析对象
        table=html.xpath('//*[@class="bluetable"]/tbody/tr')

        logger = LogUtil.get_logger('nxjd')

        for index,item in enumerate(table):
            # if index == 0:
            #     print(item.xpath(".//td/text()").extract_first())
            # if index == 1:
            #     print(item.xpath(".//td/text()").extract_first())
            tds=item.xpath(".//td")

            zy=tds[0].xpath("./text()")
            szy=str(zy)
            if(szy.endswith("次作业")):
                tm = tds[2].xpath("./text()").extract_first()
                stm=str(tm)

                stm=stm[4,5]
                stm1 = stm[4, 6]
                stm2 = stm[4, 6]


            for td in tds:
                print(td.xpath("./text()"))




