# redis
import json
import random

import redis

class RedisUtil(object):
    __pool = None

    def __init__(self):
        self.redisCon=RedisUtil.get_conn()

    @staticmethod
    def get_conn():
        if RedisUtil.__pool is None:
            __pool = redis.Redis(host='192.168.0.108',password='haohe123',port=6379,db=10,decode_responses=True)
        return __pool

    def getRandomCookie(self):
        cookies = self.redisCon.lrange("cookies",0,-1)
        if len(cookies) == 0:
            return ''

        listcookies=random.choice(cookies)
        jj=json.loads(listcookies)
        cookies_dict='{'
        for cookie in jj:
            sss = "'"+cookie['name'] + "':'" + cookie['value'] +"',"
            cookies_dict = cookies_dict+sss
        cookies_dict=cookies_dict[0:len(cookies_dict)-1]+"}"

        return cookies_dict

    def lpush(self,name,value):
        self.redisCon.lpush(name,value)

    def lrange(self, name):
        return self.redisCon.lrange(name,0,-1)