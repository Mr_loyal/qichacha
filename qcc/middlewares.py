# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html
import json
import random

from scrapy import signals
# useful for handling different item types with a single interface
from itemadapter import is_item, ItemAdapter
from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware
from scrapy.http import Headers

from qcc import settings
from qcc.RedisUtil import RedisUtil


class QccSpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, or item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request or item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class QccDownloaderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


# 随机选择 IP 代理下载器中间件
class RandomProxyMiddleware(object):
    # 从 settings 的 PROXIES 列表中随机选择一个作为代理
    def process_request(self, request, spider):
        proxy = random.choice(spider.settings['PROXIES'])
        print(proxy)
        request.meta['proxy'] = proxy
        return None

# 动态替换user-agent和cookie
class RadomUserAgentMiddleware(UserAgentMiddleware):
    def process_request(self, request, spider):
        # 随机选择agent
        user_agent_list = settings.USER_AGENTS_LIST
        agent = random.choice(user_agent_list)
        request.headers["User-Agent"] = agent

        # 企查查cookie 例子
        # cookies = {
        #     'acw_tc': 'b4a37a2816167369376712952e3ecc7912491c0db7f9a60b9585b0b56b',
        #     'CNZZDATA1254842228': '472977143-1615263367-%7C1616563623',
        #     'hasShow': '1',
        #     'QCCSESSID': 'vu3n0932lfjrn52mqtolv3go12',
        #     'UM_distinctid': '17863b5fab11a0-0dadf61d248ed2-3f616e4b-fa000-17863b5fab290f',
        #     'zg_de1d1a35bfa24ce29bbf2c7eb17e6c4f': '%7B%22sid%22%3A%201616736937563%2C%22updated%22%3A%201616738533732%2C%22info%22%3A%201616580573818%2C%22superProperty%22%3A%20%22%7B%5C%22%E5%BA%94%E7%94%A8%E5%90%8D%E7%A7%B0%5C%22%3A%20%5C%22%E4%BC%81%E6%9F%A5%E6%9F%A5%E7%BD%91%E7%AB%99%5C%22%7D%22%2C%22platform%22%3A%20%22%7B%7D%22%2C%22utm%22%3A%20%22%7B%7D%22%2C%22referrerDomain%22%3A%20%22www.qcc.com%22%2C%22cuid%22%3A%20%22260fbef807fb499d4275ce8b860b721a%22%2C%22zs%22%3A%200%2C%22sc%22%3A%200%7D',
        #     'zg_did': '%7B%22did%22%3A%20%221781569a5bf532-09d0c4d076a24b8-3f616e4b-fa000-1781569a5c0b99%22%7D'
        # }
        # 从redis中随机获取cookie
        ck=RedisUtil().getRandomCookie()
        if len(ck) != 0:
            request.cookies = eval(ck)