# 获取企查查首页新闻

import scrapy
from scrapy.selector import Selector
from lxml import etree
from qcc.items import QccItem


class NewJobSpider(scrapy.Spider):
    name = 'new_job'
    allowed_domains = ['qcc.com']
    start_urls = ['https://www.qcc.com/index_flashnewslist?ajax=true&lastRankIndex=&lastRankTime=']

    # def __init__(self):
    #     super(NewJobSpider, self).__init__()
    #
    # def start_requests(self):
    # params = {'pageCount': '100'}
    # content = requests.get('http://AAA.cc/AAA/json', params)
    # json = content.json()
    # resultJson = json["result"]

    # for result in resultJson:
    #     newUrl = result["https://www.qcc.com/index_flashnewslist?ajax=true&lastRankIndex=&lastRankTime="]
    #     print("当前抓取的 Url 是：" + newUrl)
    #     yield Request("https://www.qcc.com/index_flashnewslist?ajax=true&lastRankIndex=&lastRankTime=")

    def parse(self, response):
        news_list = response.xpath("//div[@class='flnews-cell']")
        date = response.xpath("//div[@class='flnews-date']//text()").get()
        date=date.replace('年','-').replace('月','-').replace('日',' ')
        print(date)
        for item in news_list:
            title = item.xpath("normalize-space(.//div[@class='item']//div[@class='title']//text())").extract_first()
            content = item.xpath("normalize-space(.//div[@class='item']//div[@class='content']//text())").extract_first()
            time = item.xpath("normalize-space(.//div[@class='tline']//span[@class='time']//text())").extract_first()

            qcc_item = QccItem()
            qcc_item['title'] = title
            qcc_item['content'] = content
            qcc_item['dateTime'] = date+time
            yield qcc_item
