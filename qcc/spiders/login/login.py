# 登录企查查

import json

import scrapy

from qcc import settings
from qcc.MysqlUtil import MysqlUtil
from qcc.RedisUtil import RedisUtil
from qcc.SelenuimUitl import SelenuimUitl


class LoginSpider(scrapy.Spider):
    name = 'login'
    allowed_domains = ['qcc.com']
    start_urls = ['https://qcc.com/']

    def parse(self, response):
        redis_pool = RedisUtil()
        users = settings.USER_ACCOUNT
        for user in users:
            u = user.split('-')
            # 登录企查查获取cookie
            su = SelenuimUitl()
            listcookies = su.login(u[0], u[1])
            # cookie 保存
            redis_pool.lpush('cookies', json.dumps(listcookies))



class XnjdSpider(scrapy.Spider):
    name = 'xnjd'
    allowed_domains = ['xnjd.cn']
    start_urls = ["https://www.xnjd.cn/"]

    # def start_requests(self):
    #     cookie = "JSESSIONID=08A21010042CAC08C25DE0CBD4676CE8"
    #     yield scrapy.Request('https://www.xnjd.cn/', callback=self.parse, cookies=cookie)

    def parse(self, response):
            su = SelenuimUitl()
            su.xnjdClick()
