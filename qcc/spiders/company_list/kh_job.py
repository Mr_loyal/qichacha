# 企查查企业列表

import json
from urllib.request import Request
from scrapy.selector import Selector
from lxml import etree

import scrapy

from qcc.RedisUtil import RedisUtil


class KhJobSpider(scrapy.Spider):
    name = 'kh_job'
    allowed_domains = ['qcc.com']
    start_urls = []

    # 重载start_requests方法
    def start_requests(self):
        # cookie 字符串案例 转为字典既可用
        # cookie="{'acw_tc' : '4a37a1f16165665189781404ea95aa90c0b86d03e085cd7ba447bf3a0', 'CNZZDATA1254842228' : '472977143-1615263367-%7C1616563623','hasShow': '1','QCCSESSID':'vu3n0932lfjrn52mqtolv3go12','UM_distinctid':'1783f5643d29b8-00aa6c350af4328-3f616e4b-fa000-1783f5643d395d','zg_de1d1a35bfa24ce29bbf2c7eb17e6c4f':'%7B%22sid%22%3A%201616566519697%2C%22updated%22%3A%201616566559921%2C%22info%22%3A%201615970321415%2C%22superProperty%22%3A%20%22%7B%5C%22%E5%BA%94%E7%94%A8%E5%90%8D%E7%A7%B0%5C%22%3A%20%5C%22%E4%BC%81%E6%9F%A5%E6%9F%A5%E7%BD%91%E7%AB%99%5C%22%7D%22%2C%22platform%22%3A%20%22%7B%7D%22%2C%22utm%22%3A%20%22%7B%7D%22%2C%22referrerDomain%22%3A%20%22%22%2C%22cuid%22%3A%20%22260fbef807fb499d4275ce8b860b721a%22%2C%22zs%22%3A%200%2C%22sc%22%3A%200%7D','zg_did':'%7B%22did%22%3A%20%221781569a5bf532-09d0c4d076a24b8-3f616e4b-fa000-1781569a5c0b99%22%7D'}"

        url='https://www.qcc.com/api/search/searchMulti'
        form_data = {"searchKey":"{\"name\":\"上海\"}","pageIndex":1,"pageSize":20,"searchIndex":"multicondition"}

        yield scrapy.FormRequest(url,body=json.dumps(form_data),method='POST',callback=self.parse)


    def parse(self, response):
       s=json.loads(response.body)
       print(s['Result'])
