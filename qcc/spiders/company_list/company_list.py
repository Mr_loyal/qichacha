import json
from urllib.request import Request
from scrapy.selector import Selector
from lxml import etree
from scrapy.utils.log import configure_logging
import scrapy

from qcc import LogUtil
from qcc.RedisUtil import RedisUtil
from qcc.SelenuimUitl import SelenuimUitl
from qcc.items import CompanylistItem


# 按区域查询公司-公司列表
class AreaCompanySpider(scrapy.Spider):
    name = 'area_company'
    allowed_domains = ['qcc.com']
    start_urls = []

    def start_requests(self):
        redis_pool = RedisUtil()
        # 获取redis中的所有区域的区域代码
        areaStr = redis_pool.lrange('areaList')
        # areaStr=['/g_AH_340600']
        for areaStr in areaStr:
            areaList = areaStr.split(',')
            for area in areaList:
                area = area.replace('"', '')
                # url拼接地区代码
                i = 1
                # 循环所有地区 查询公司列表
                while i < 501:
                    logger=LogUtil.get_logger('area_company')
                    logger.info('区域：' + area + " 页数：" + str(i))
                    url = 'https://www.qcc.com' + area + '_' + str(i)
                    i = i + 1
                    yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        try:
            item = CompanylistItem()
            print(response)
            searchlist = response.xpath('//*[@id="searchlist"]')
            if searchlist != None:
                trList = response.xpath('//*[@id="searchlist"]/table/tbody/tr')
                print('数量:' + str(len(trList)))
                if len(trList) != 0:
                    for tr in trList:
                        item['logo'] = tr.xpath(".//td[1]/img/@src").extract_first()
                        item['company_name'] = tr.xpath("normalize-space(.//td[2]/a//text())").extract_first()
                        item['fddbr'] = tr.xpath("normalize-space(.//td[2]/p[1]/a//text())").extract_first()
                        zczb = tr.xpath("normalize-space(.//td[2]/p[1]/span[1]//text())").extract_first()
                        item['zczb'] = str(zczb).split('：')[1]
                        item['clsj'] = tr.xpath("normalize-space(.//td[2]/p[1]/span[2]//text())").extract_first()
                        yx = tr.xpath("normalize-space(.//td[2]/p[2]//text())").extract_first()
                        item['email'] = str(yx).split('：')[1]
                        dh = tr.xpath("normalize-space(.//td[2]/p[2]/span//text())").extract_first()
                        item['dh'] = str(dh).split('：')[1]
                        dz = tr.xpath("normalize-space(.//td[2]/p[3]//text())").extract_first()
                        item['dz'] = str(dz).split('：')[1]
                        item['jyzt'] = tr.xpath("normalize-space(.//td[3]/span//text())").extract_first()
                        item['detail_url'] = 'https://www.qcc.com' + tr.xpath(".//td[2]/a//@href").extract_first()
                        yield item
        except Exception as r:
            print('未知错误 %s' % (r))


# 查询最新注册的公司-公司列表
class LastCompanySpider(scrapy.Spider):
    name = 'last_company'
    allowed_domains = ['qcc.com']
    start_urls = ['https://www.qcc.com']

    def parse(self, response):
        su = SelenuimUitl()
        self.browser=su.area()
        rp=self.browser.page_source
        trList = rp.xpath("//div[@class='msearch']/table/tr")
        for tr in trList:
            item = CompanylistItem()
            item['logo'] = tr.xpath("./td[1]/div/img/@src").extract_first()
            item['company_name'] = trList.xpath("normalize-space(.//td[2]/a//text())").extract_first()
            item['fddbr'] = trList.xpath("normalize-space(.//td[2]/p[1]/a//text())").extract_first()

            zczb = trList.xpath("normalize-space(.//td[2]/p[1]/span[1]//text())").extract_first()
            item['zczb'] = str(zczb).split('：')[1]

            item['clsj'] = trList.xpath("normalize-space(.//td[2]/p[1]/span[2]//text())").extract_first()

            yx = trList.xpath("normalize-space(.//td[2]/p[2]//text())").extract_first()
            item['email'] = str(yx).split('：')[1]

            dh = trList.xpath("normalize-space(.//td[2]/p[2]/span//text())").extract_first()
            item['dh'] = str(dh).split('：')[1]

            dz = trList.xpath("normalize-space(.//td[2]/p[3]//text())").extract_first()
            item['dz'] = str(dz).split('：')[1]

            item['jyzt'] = trList.xpath("normalize-space(.//td[3]/span//text())").extract_first()
            item['detail_url'] = trList.xpath(".//td[2]/a/@href").extract_first()

            yield item



# 企查查首页搜索地方、selnuim点击企业名搜索
class SearchCompanySpider(scrapy.Spider):
    name = 'search_company'
    allowed_domains = ['qcc.com']
    start_urls = ['https://www.qcc.com']

    # def start_requests(self):
    #     yield scrapy.Request('https://www.qcc.com', callback=self.parse)

    def parse(self, response):
        su = SelenuimUitl()
        su.qymClick()
