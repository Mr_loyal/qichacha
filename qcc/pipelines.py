# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from qcc import LogUtil
from qcc.MysqlUtil import MysqlUtil
from qcc.spiders.company_detail_list.company_detail import GSXXSpider, ZYRYSpider, GDXXSpider
from qcc.spiders.company_list.company_list import AreaCompanySpider
from qcc.spiders.company_list.kh_job import KhJobSpider
from qcc.spiders.news.new_job import NewJobSpider


class QccPipeline:
    pool = None

    def __init__(self):
        pass

    def open_spider(self, spider):
        self.pool = MysqlUtil()

    def process_item(self, item, spider):
        if isinstance(spider, NewJobSpider):
            print("新闻任务")
            # 判断文章是否存在
            # 插入文章
            self.pool.insert_one('insert into qcc_news(date,title,content) values(%s,%s,%s)',
                                 (item['dateTime'], item['title'], item['content']))
            self.pool.end("commit")
        elif isinstance(spider, KhJobSpider):
            print("客户任务")
            pass
        elif isinstance(spider, AreaCompanySpider):
            # 按区域查询 公司列表
            if ('公司' in item['company_name']) | ('中心' in item['company_name']) | ('工作室' in item['company_name']):
                # 查询公司是否存在
                result = self.pool.get_all(
                    'select * from qcc_company_list where company_name=' + "'" + item['company_name'] + "'")
                if not result:
                    # 插入公司列表
                    result=self.pool.insert_one(
                        'insert into qcc_company_list(logo,company_name,fddbr,zczb,clsj,dh,email,dz,jyzt,detail_url) '
                        'values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
                        (item['logo'], item['company_name'], item['fddbr'], item['zczb'], item['clsj'], item['dh'],
                         item['email'], item['dz'], item['jyzt'],item['detail_url']))
                    self.pool.end("commit")
                    logger = LogUtil.get_logger('area_company')
                    if result>0:
                        logger.info('插入成功：'+ item['company_name'] )
                    else:
                        logger.info('插入失败：'+ item['company_name'] )
        elif isinstance(spider, GSXXSpider):
            print("工商信息")

            result = self.pool.get_all(
                    'select * from qcc_gsxx where company_id=' + str(item['company_id']))
            if not result:
                result=self.pool.insert_one(
                        'insert into qcc_gsxx(company_id,company_name,fddbr,djzt,clrq,zczb,sjzb,hzrq,xydm,jgdm,'
                        'gszch,nsrsbh,qydm,sshy,qylx,yyqx,djjg,rygm,cbrs,ssdq,cym,ywm,qydz,jyfw) '
                        'values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
                        (item['company_id'],
                         item['company_name'],item['fddbr'],item['djzt'],item['clrq'],item['zczb'],item['sjzb'],
                         item['hzrq'],item['xydm'],item['jgdm'],item['gszch'],item['nsrsbh'],item['qydm'],item['sshy'],
                         item['qylx'],item['yyqx'],item['djjg'],item['rygm'],item['cbrs'],item['ssdq'],item['cym'],
                         item['ywm'],item['qydz'],item['jyfw']))
                self.pool.end("commit")
        elif isinstance(spider, GDXXSpider):
            print("股东信息")

            result = self.pool.get_all(
                    'select * from qcc_gdxx where company_id=' + str(item['company_id']))
            if not result:
                    # 插入股东信息
                self.pool.insert_one(
                        'insert into qcc_gdxx(company_id,gdmc,cgbl,rjcze,rjczrq) '
                        'values(%s,%s,%s,%s,%s)',
                        (item['company_id'],item['gdmc'],
                         item['cgbl'],item['rjcze'],item['rjczrq']))
                self.pool.end("commit")

        elif isinstance(spider, ZYRYSpider):
            print("主要人员信息")

            result = self.pool.get_all(
                    'select * from qcc_zyry where company_id=' + str(item['company_id']))
            if not result:
                    # 插入主要人员信息
                self.pool.insert_one(
                        'insert into qcc_zyry(company_id,name,sex,xl,job,xc,cgs,cgbl,sygf,bjrq,ggrq)'
                        'values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
                        (item['company_id'],
                         item['name'],item['sex'],item['xl'],item['job']
                         ,item['xc'],item['cgs'],item['cgbl'],item['sygf'],
                         item['bjrq'],item['ggrq']))
                self.pool.end("commit")

        return item  # 必须实现返回

    def close_spider(self, spider):
        pass
